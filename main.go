package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gocolly/colly"
	"os"
	"regexp"
	"strings"
)

var err error

var offer Offer

// Offer type of offer item
type Offer struct {
	offerID        string
	newBuilding    string
	regionID       string
	sellerName     string
	sellerType     string
	sellerCreateAt string
	createdAt      string
	offerType      string
	link           string
	developer      string
	deadline       string
	address        string
	price          string
	roomNumber     string
	square         string
	floor          string
	maxFloor       string
}

var previousUrl = ""

var stopPagination = false

func main() {

	//regionID := "5020"
	regionID := "1"
	url := "https://tula.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&region=" + regionID

	c := colly.NewCollector(
		//colly.Debugger(&debug.LogDebugger{}),
		colly.UserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"),
	)

	setHeaders(c)

	c.OnResponse(func(r *colly.Response) {

		if previousUrl != "" && previousUrl == r.Request.URL.String() {
			fmt.Println("Stop pagination ...")
			stopPagination = true
		}

		fmt.Println("Visited", r.Request.URL, "Code:", r.StatusCode)

		isCaptcha := strings.Contains(r.Request.URL.String(), "captcha")

		if isCaptcha {
			fmt.Println("There is captcha")
			os.Exit(2)
		}

		previousUrl = r.Request.URL.String()
	})

	c.OnHTML(itemSelector, func(e *colly.HTMLElement) {

		offer.regionID = regionID

		offer.link = getLink(e)

		re := regexp.MustCompile("[0-9]+")
		strID := re.FindAllString(offer.link, 1)
		offer.offerID = strID[0]

		offer.developer = getDeveloper(e)
		offer.deadline = getDeadline(e)
		offer.address = getGeoLocation(e)

		offer = getTitleInfo(e, offer)
		offer = getPrice(e, offer)
		offer = getSeller(e, offer)

		insertOffers(offer)
	})

	c.OnError(func(r *colly.Response, err error) {

		fmt.Println("Request URL:", r.Request.URL, "failed with response:", "\nError:", err, "Response Code:", r.StatusCode)
	})

	visit(c, url)
}
