package main

import (
	"github.com/gocolly/colly"
	"math/rand"
	"strconv"
	"time"
)

const maxPageNumber = 60
const minSleep int = 1
const maxSleep int = 3
const priceStep int = 500000

var minPrice = 0

func visit(c *colly.Collector, originalURL string) {

	visitByBuildingType(c, originalURL)
}

func visitByBuildingType(c *colly.Collector, originalURL string) {

	buildingTypes := []int{1,2} // 1 - old building, 2 - new building

	for _, t := range buildingTypes {

		offer.newBuilding = "0"

		if t == 2 {
			offer.newBuilding = "1"
		}

		visitByPrice(c, originalURL, t)
	}
}

func visitByPrice(c *colly.Collector, originalURL string, buildingType int) {

	minPrice = 0

	for i := 1; i <= 50; i++ {

		stopPagination = false

		maxPrice := i * priceStep
		visitByPages(c, originalURL, buildingType, maxPrice)
	}
}

func visitByPages(c *colly.Collector, originalURL string, buildingType int, maxPrice int) {

	for page := 1; page <= maxPageNumber; page++ {

		if stopPagination {
			break
		}

		url := originalURL +
			"&object_type%5B0%5D=" + strconv.Itoa(buildingType)  +
			"&minprice=" + strconv.Itoa(minPrice) +
			"&maxprice=" + strconv.Itoa(maxPrice) +
			"&p=" + strconv.Itoa(page)

		c.Visit(url)
		randSleep()
	}

	minPrice = maxPrice
}

func randSleep()  {

	randInt := rand.Intn(maxSleep) + minSleep
	duration := time.Duration(randInt) * time.Second
	time.Sleep(duration)
}
