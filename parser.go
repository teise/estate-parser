package main

import (
	"strings"

	"github.com/gocolly/colly"
)

const itemSelector = "article[data-name=\"CardComponent\"]"

func getGeoLocation(e *colly.HTMLElement) string {

	var region string = e.ChildText("[data-name=\"GeoLabel\"]:nth-child(1)")
	var city string = e.ChildText("[data-name=\"GeoLabel\"]:nth-child(2)")
	var town string = e.ChildText("[data-name=\"GeoLabel\"]:nth-child(3)")
	var street string = e.ChildText("[data-name=\"GeoLabel\"]:nth-child(4)")
	var haus string = e.ChildText("[data-name=\"GeoLabel\"]:nth-child(5)")

	geoLocation := region + ", " + city + ", " + town + ", " + street + ", " + haus

	return geoLocation
}

func getSeller(e *colly.HTMLElement, offer Offer) Offer {

	offer.sellerName = e.ChildText("[data-name=\"AgentTitle\"]")
	offer.sellerType = e.ChildText("[data-name=\"Agent\"] [data-name=\"ContentRow\"] div")
	offer.sellerCreateAt = e.ChildText("[data-name=\"AgentTitleContainer\"]")

	return offer
}

func getTitleInfo(e *colly.HTMLElement, offer Offer) Offer {

	title := e.ChildText("div[data-name=\"TitleComponent\"]")
	arr := strings.Split(title, ",")

	if len(arr) >= 4 {

		floors := arr[3]
		arFloor := strings.Split(floors, "/")

		if len(arFloor) > 0 {
			offer.floor = strings.TrimSpace(arFloor[0])
		}

		if len(arFloor) > 1 {

			if len(arFloor[1]) > 2 {
				offer.maxFloor = strings.TrimSpace(arFloor[1][:2])
			}
		}
	}

	if len(arr) >= 1 {
		offer.roomNumber = strings.TrimSpace(arr[0][:1])
	}
	if len(arr) >= 3 {
		offer.square = strings.TrimSpace(arr[1])

		if len(arr[2]) > 0 {
			offer.square += strings.TrimSpace("." + arr[2][:1])
		}
	}
	return offer
}

func getPrice(e *colly.HTMLElement, offer Offer) Offer {

	price := e.ChildText("span[data-mark=\"MainPrice\"]")
	price = strings.Replace(price, "₽", "", -1)
	offer.price = strings.Replace(price, " ", "", -1)

	return offer
}

func getDeveloper(e *colly.HTMLElement) string {
	return e.ChildText("div[data-name=\"ContentRow\"] div[data-name=\"ContentRow\"] a")
}

func getDeadline(e *colly.HTMLElement) string {
	return e.ChildText("span[data-mark=\"Deadline\"]")
}

func getLink(e *colly.HTMLElement) string {
	return e.ChildAttr("a", "href")
}
