package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"
)

const sleepBunTime = 43200 // 12 hours

var db = dbConn()

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "testpass"
	dbName := "cian-estate"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}

	return db
}

func insertOffers(offer Offer) {
	db := dbConn()
	importedAt := strconv.FormatInt(time.Now().Unix(), 10)

	offerSql := "INSERT IGNORE INTO offers (" +
		"offer_id, " +
		"new_building, " +
		"region_id, " +
		"seller_name, " +
		"seller_type, " +
		"seller_created_at, " +
		"imported_at, " +
		"type, " +
		"link, " +
		"developer, " +
		"deadline, " +
		"address, " +
		"price, " +
		"room_number, " +
		"square, " +
		"floor, " +
		"max_floor) " +
		"VALUES " +
		"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

	stmt, err := db.Prepare(offerSql)

	checkErr(err)

	_, err = stmt.Exec(
		offer.offerID,
		offer.newBuilding,
		offer.regionID,
		offer.sellerName,
		offer.sellerType,
		offer.sellerCreateAt,
		importedAt,
		"1",
		offer.link,
		offer.developer,
		offer.deadline,
		offer.address,
		offer.price,
		offer.roomNumber,
		offer.square,
		offer.floor,
		offer.maxFloor,
	)
	checkErr(err)

	defer db.Close()
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func getLastLog() {

	sql := "SELECT log_id, code FROM logs LIMIT 1"

	row, err := db.Query(sql)
	checkErr(err)

	fmt.Println(row)
}
