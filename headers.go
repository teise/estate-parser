package main

import (
	"github.com/gocolly/colly"
	"io/ioutil"
)

func setHeaders(c *colly.Collector) {

	c.OnRequest(func(r *colly.Request) {

		cookies, err := ioutil.ReadFile("cookies.txt")
		if err != nil {
			checkErr(err)
		}

		cookiesStr := string(cookies) // convert content to a 'string'

		r.Headers.Set("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
		r.Headers.Set("accept-encoding", "gzip, deflate, br")
		r.Headers.Set("accept-language", "en-GB,en-US;q=0.9,en;q=0.8,ru;q=0.7")
		r.Headers.Set("cache-control", "no-cache")
		r.Headers.Set("cookie", cookiesStr)
		r.Headers.Set("pragma", "no-cache")
		r.Headers.Set("refere", "https://www.cian.ru/")
		r.Headers.Set("sec-fetch-dest", "document")
		r.Headers.Set("sec-fetch-mode", "navigate")
		r.Headers.Set("sec-fetch-site", "same-site")
		r.Headers.Set("sec-fetch-user", "?1")
		r.Headers.Set("upgrade-insecure-requests", "1")
	})
}
